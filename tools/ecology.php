#!/usr/bin/php5 -q
<?php

/*****************************************************
 *
 * finds lists which are 'dead' using these criteria:
 * - lists which have been closed for more than $closed_age months
 * - lists with no new archives for the last $dormant_age months
 *   
 ****************************************************/


 // CONFIGURATION VARIABLES

$home = "/home/sympa";
$bin  = "$home/bin";
$expl = "$home/expl";
$arc  = "$home/arc";
$cemetery    = "$home/cemetery";
$defaultdomain = 'lists.riseup.net';
$closed_age  = 4;
$dormant_age = 18;
$config_change_age = 6;

$closed_datafile  = dirname(__FILE__) . "/data/closedlists";
$dormant_datafile = dirname(__FILE__) . "/data/dormantlists";				      

$max = 30; # how many to lists show at a time

# the from address for the close list notice. 
$from = "no-reply@riseup.net";

# the close list notice send to list owners
# do not put single or double quotes in this string!!
$close_msg = 'Hello,

Your list appears to be dormant:

   $list@$domain

It has not had any traffic for over $dormant_age months, so we have
disabled it. To save space, it will be removed in four more
months. 

If you still want the list, visit https://user.riseup.net,
create a help ticket, and ask for your list to be restored. 

You should still be able to login and download the list
archives:

- Login at http://$domain
 - Go to http://$domain/www/admin/$list
 - Click Manage Archives.

In solidarity,
the riseup collective';

$close_msg_to_listmaster = 'We are closing the list $list@$domain as
it appears to be  dormant. 

This message is going to the listmaster as the list does not appear to have an active
administrator.';

$listmaster =  'listmaster@lists.riseup.net';

##
## CODE BEGINS HERE
## 

$argv = &$_SERVER['argv'];
$argc = &$_SERVER['argc'];

$main = array(
	'updateclosed'  => 'Closed lists: update data.',
	'autoremove'    => 'Closed lists: remove all.',
	'updatedormant' => 'Dormant lists: update data.',
	'autodormant'   => 'Dormant lists: close all.',
	//'quit'          => 'Quit'
);

if ( ( $argc < 2) or (($argc > 1) and !array_key_exists($argv[1],$main))) {
  echo "You must give one of the possible actions as a paramter. Here they are:\n"; 
  foreach (array_keys($main) as $action) {
    echo "$action\n";
  }
  exit;
}

$action = $argv[1];

	if ($action == 'updateclosed') {
		do_update_closed();
		//echo "Finished. Hit return to continue\n";
		//`read`;
	}
	elseif ($action == 'updatedormant') {
		do_update_dormant();
		//echo "Finished. Hit return to continue\n";
		//`read`;
	}
	elseif ($action == 'autodormant') {
		$lists = file($dormant_datafile);
		foreach ($lists as $list) {
			$list=trim($list);
			if (empty($list)) continue;
			echo("$list: \n");
			echo(trim(`if [ -e $arc/$list ]; then ls -C $arc/$list; else echo none; fi`));
			echo("\n");
			close_list($list);
			sleep(1);
		}
	} 
	elseif ($action == 'autoremove') {
		$lines = file($closed_datafile);
		foreach ($lines as $line) {
			list($list, $time) = explode(' ',trim($line));
			list($list, $domain) = explode('@', $list);
			echo("\n$list@$domain: ");

			$data = read_array($closed_datafile);
			unset($data["$list@$domain"]);
			write_array($data, $closed_datafile);

			//#`[ -d $expl/$domain/$list/ ] && mv -v $expl/$domain/$list/ $cemetery/$list@$domain`;

			`[ -d $expl/$list/ ] && mv -v $expl/$list/ $cemetery/$list@$domain`;
			`[ -d $arc/$list@$domain/ ] && mv -v $arc/$list@$domain/ $cemetery/$list@$domain/arc`;

			echo("removed.");
			sleep(1);
		}
	}
//}

//return;


/********
 *
 * updates the data files for old, closed, and dormant lists
 *
 ********/
 
function do_update_closed() {
	global $expl, $closed_datafile, $closed_age, $defaultdomain;
	
	$old = array();
	$closed = `find $expl -follow -maxdepth 3 -type f -name config | xargs grep -l "^status closed"`;
	$closed = split("\n",trim($closed));
	$fourmonthsago = mktime (0,0,0,date("m")-$closed_age,date("d"),date("Y"));
	foreach($closed as $config) {
		$domain = basename(dirname(dirname($config)));
		$list = basename(dirname($config));
		if ($domain == 'expl') {
			$domain = $defaultdomain;
		}
		echo "$list@$domain\n";
		$updatetime = get_time($config,'update');
		if ($updatetime == 0)
			$updatetime = get_time($config,'creation');

		if ($updatetime < $fourmonthsago)
			$old["$list@$domain"] = date("m.d.y", $updatetime);
	}
	write_array($old, $closed_datafile);
	//backup with date
	copy($closed_datafile, $closed_datafile.date('Y-m-d_H:i')); 
}

function do_update_dormant() {
	global $expl, $arc, $dormant_datafile, $dormant_age, $config_change_age, $defaultdomain;

	$dormant = array();
	// create a list of the last $dormant_age months
	for($i=0; $i<=$dormant_age; $i++) {
		$months[] = date("Y-m", mktime (0,0,0,date("m")-$i,date("d"),date("Y")) );
	}
	$oldtime = mktime(0,0,0,date("m")-$config_change_age,date("d"),date("Y"));
	$lists = `find $expl -mindepth 1 -maxdepth 1 -follow -type d -printf "%f\n"`; //look in expl rather than arc to include lists that have never have a message sent
	//TODO: This includes closed lists, which is not ideal
	$lists = split("\n",trim($lists));
	$domain = $defaultdomain;

	//$lists = array('mail@lists.riseup.net');
	foreach($lists as $list) {
	  if (in_array($list, array('listadmins', 'mailusers'))) {
	    continue;
	  }

		$found = false;
		//list($list, $domain) = explode('@', $list); #domain not include in /home/sympa/expl
		foreach($months as $month) {
			if (is_dir("$arc/$list@$domain/$month")) {
				$found = true;
				//#echo "found $arc/$list@$domain/$month\n";
				break;
			}
		}
		//TODO: want to check for lists that have archives disabled. see:
		//https://labs.riseup.net/code/issues/9693
		if ($found == false) {
		  //$config = "$expl/$domain/$list/config";
			$config = "$expl/$list/config";
			if (file_exists($config)) {
				// skip over lists which have had their configs modified.
				//echo `ls -l $expl/$domain/$list/config`;
				$updatetime = get_time($config);
				if ($updatetime > $oldtime) {
					echo "$list@$domain: skipping because config changed\n";
					continue;
				}
			}
			$dormant[] = "$list@$domain\n";
			echo "$list@$domain: dormant\n";
		}
	}
	write_array($dormant, $dormant_datafile, false);
	//backup with date
	copy($dormant_datafile, $dormant_datafile.date('Y-m-d_H:i')); 
}


///## HELPER FUNCTION


function write_array($array, $filename, $writekeys=true) {
	$out = fopen($filename, 'w');
	if ($out === false) die("Could not open $filename\n");
	if ($writekeys) {
		foreach($array as $key => $value) {
    		        fwrite($out, "$key $value\n");
		}
	}
	else {
		foreach($array as $value) {
			fwrite($out, "$value\n");
		}
	}
	fclose($out);
}

function read_array($filename, $writekeys=true) {
	$data = file($filename);
	if (!$writekeys) {
		foreach($data as $i) {
			$array[] = trim($i);
		}
		return $array;
	}
	else {
		$array = array();
		foreach($data as $i) {
			list($key, $value) = explode(' ',trim($i));
			if ($key != '' && $value != '')
				$array[$key] = $value;
		}
		return $array;
	}
}


/** 
 *
 * looking for:
 *  update
 *  date_epoch 1070329881
 *  date 01 Dec 2003 at 17:51:21
 *  email elijah@riseup.net
 *
 **/

function get_time($configfile, $stanza='update') {
	$f = fopen($configfile,'r');
	while ($f && !feof($f)) {
		$line = trim(fgets($f,2048));
		if ($line == $stanza) {
			while (!feof($f)) {
	   			$line = trim(fgets($f,2048));
				if ($line == '')
					break;
				elseif(preg_match("/^\s*date_epoch (.*)$/", $line,$matches))
					return $matches[1];
			}
		}
	}
	return '0';
}

## 
## returns all owners and editors of a list
##

function get_admins($file) {
	if (!file_exists($file)) return;

	$admins = array();	
	$f = fopen($file,'r');
	while ($f && !feof($f)) {
		$line = trim(fgets($f,2048));
		if ($line == 'owner' || $line == 'editor') {
			while (!feof($f)) {
		 	   $line = trim(fgets($f,2048));
				if ($line == '')
					break;
				elseif(preg_match("/^\s*email (.*)$/", $line,$matches)) {
					$admins[] = $matches[1];
					break;
				}
			}
		}
	}
	$admins = array_unique($admins);
	return $admins;
}

function remove_from_datafile($datafile, $text) {
	$data = read_array($datafile, false);
	$data = array_flip($data);
	unset($data[$text]);
	$data = array_flip($data);
	write_array($data, $datafile, false);
}

/* takes list as listname@domain */


function close_list($list) {
	global $expl, $close_msg, $dormant_age, $dormant_datafile, $arc, $cemetery, $bin, $from;

	list($list, $domain) = explode('@', trim($list));
	//#$admins = get_admins("$expl/$domain/$list/config");
	$admins = get_admins("$expl/$list/config");
	
	//* It seems like this moved the archives for dormant lists
	//* w/no admins, and did not move the settings.
	//* At least for now, we will close the dormant-no-admin lists,
	//* and send an email to listmaster.
	
	// if (count($admins) == 0) { 
//		# no config//
//		echo ". moving archives to cemetery.";
//		`mv $arc/$list@$domain/ $cemetery/arc-$list@$domain`;
//	}/
//	else { 

        $ok = system("$bin/sympa.pl --close_list $list@$domain");
	echo "\n";
	if ($ok === FALSE) {
	  die("Could not call sympa.pl --close_list\n");
	}  else {
	  if  (count($admins) == 0) {
	    eval("\$str = \"$close_msg_to_listmaster\";");	 
	    mail($listmaster, "List Closed:$list@$domain", $str,"From: $from");
	  } else {
	    eval("\$str = \"$close_msg\";");
	    mail(join(',',$admins), "List Closed: $list@$domain", $str,"From: $from");
	  }
	}
	//}
	remove_from_datafile($dormant_datafile, "$list@$domain");
}

?>
