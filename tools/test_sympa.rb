require 'minitest/spec'
require 'minitest/autorun'
require_relative 'sympa_test_suite'

$list = 'testlist-' + (0..8).map{ rand(10) }.join

describe "wwsympa" do

	def self.test_order
		:alpha
	end

	before do
	end

	after do
		# Log out the mech instance
	end

	describe "listmaster is logged in" do

		it "001 can create a list" do
			sts = SympaTestSuite.new
			sts.login
			page = sts.browser.post(sts.url, {
				:listname => $list,
				:template => "confidential",
				:subject => $list,
				:topics => "arts",
				:info => $list,
				:action_create_list => "Submit+your+creation+request",
			})
			match = $list + " - " + $list + " - create_list"
			page.parser.css('title').to_s.must_match /#{match}/
		end

		# 01_rename_lists_no_pending.patch
		# https://labs.riseup.net/code/issues/4066
		# prevents lists going in to 'pending' status if they are renamed.
		it "002 can rename list and list not change to state pending" do
			sts = SympaTestSuite.new
			sts.login
			page = sts.browser.post(sts.url, {
				:list => $list,
				:new_listname => $list + "-new",
				:new_robot => sts.robot,
				:action_rename_list => "Rename this list",
			})

			match = $list + "-new" +' - '+ $list
			page.parser.css('title').to_s.must_match /#{match}/

			page = sts.browser.get(sts.url + '/get_pending_lists')
			page.parser.css('.block form fieldset').to_s.wont_match /#{$list}/
		end

		# 02_disable_copy_list.patch
		# https://labs.riseup.net/code/projects/lists/activity?from=2012-10-05
		# Prevents copying lists
		it "003 can not copy a list" do
			sts = SympaTestSuite.new
			sts.login
			page = sts.browser.post(sts.url, {
				:list => $list + "-new",
				:new_listname => $list + "-copy",
				:new_robot => sts.robot,
				:action_copy_list => "copy list configuration",
			})

			page.parser.css('#ErrorMsg').to_s.must_match /INTERNAL SERVER ERROR/
		end

		# 03_disable_latest_and_active_lists.patch
		# rss requests to active_lists and latest_lists crash wwsympa. 
		# First test /active_lists?for=1&count=1, it should return an
		# empty list, even after the list above is added
		it "004 can not list active lists" do
			sts = SympaTestSuite.new
			sts.login
			page = sts.browser.get(sts.url + '/active_lists?for=1&count=1')

			page.parser.css('table.listOfItems').to_s.wont_match /#{$list}/
		end

		# 03_disable_latest_and_active_lists.patch
		# rss requests to active_lists and latest_lists crash wwsympa. 
		# Second test /latest_lists?for=1&count=1, it should return an
		# empty list, even after the list above is added
		it "005 can not list latest lists" do
			sts = SympaTestSuite.new
			sts.login
			page = sts.browser.get(sts.url + '/latest_lists?for=1&count=1')

			page.parser.css('table.listOfItems').to_s.wont_match /#{$list}/
		end

		# This is for issue #1187: https://labs.riseup.net/code/issues/1187
#		it "can not add name on the user preferences page" do
#			page = @browser.post(@url, {
#				:gecos => "Mesuir Burf Letet",
#				:lang => "en_US",
#				:cookie_delay => "0",
#				:action_setpref => "Submit",
#			})
#
#			page.content.match(/Mesuir Burf Letet/).must_be_nil
#		end
#
#		it "can add a user to a list" do
#			page = @browser.post(@url, {
#				:list => @@list,
#				:action => "add",
#				:email => @user,
#				:action_add => "Add",
#			})
#			page.content.must_match /1 subscribers added/
#		end
#
#		it "can add many users to a list" do
#			page = @browser.post(@url, {
#				:list => @@list,
#				:used => "true",
#				:dump => "test1@meow.com\ntest2@meow.com\ntest3@meow.com",
#				:action_add => "Add",
#			})
#			page.content.must_match /3 subscribers added/
#		end
#
#		it "can delete a user from a list" do
#			page = @browser.post(@url, {
#				:list => @@list,
#				:action_del => "Delete+selected+email+addresses",
#				:email => @user
#			})
#			page.content.must_match /1 addresses have been removed/
#		end
#
#		it "can delete a list" do
#			page = @browser.post(@url, {
#				:list => @@list,
#				:action_close_list => "Remove+List",
#			})
#			page.content.must_match /List has been closed/
#		end
#
#		# This is for issue #6404: https://labs.riseup.net/code/issues/6404
#		it "can not add a list with a name that ends in -admin" do
#			listname = @@list +"-admin"
#			page = @browser.post(@url, {
#				:listname => listname,
#				:template => "confidential",
#				:subject => listname,
#				:topics => "arts",
#				:info => listname,
#				:action_create_list => "Submit+your+creation+request",
#			})
#
#			if page.content.match(/Your list is created/)
#				@browser.post(@url, {
#					:list => listname,
#					:action_close_list => "Remove+List",
#				})
#			end
#				
#			page.content.match(/Your list is created/).must_be_nil
#		end
#
#		# This is for issue #6404: https://labs.riseup.net/code/issues/6404
#		it "can not add a list with a name that ends in -owner" do
#			listname = @@list +"-owner"
#			page = @browser.post(@url, {
#				:listname => listname,
#				:template => "confidential",
#				:subject => listname,
#				:topics => "arts",
#				:info => listname,
#				:action_create_list => "Submit+your+creation+request",
#			})
#
#			if page.content.match(/Your list is created/)
#				@browser.post(@url, {
#					:list => listname,
#					:action_close_list => "Remove+List",
#				})
#			end
#				
#			page.content.match(/Your list is created/).must_be_nil
#		end
#
#		# This is for issue #6404: https://labs.riseup.net/code/issues/6404
#		it "can not add a list with a name that ends in -request" do
#			listname = @@list +"-request"
#			page = @browser.post(@url, {
#				:listname => listname,
#				:template => "confidential",
#				:subject => listname,
#				:topics => "arts",
#				:info => listname,
#				:action_create_list => "Submit+your+creation+request",
#			})
#
#			if page.content.match(/Your list is created/)
#				@browser.post(@url, {
#					:list => listname,
#					:action_close_list => "Remove+List",
#				})
#			end
#				
#			page.content.match(/Your list is created/).must_be_nil
#		end


		# TODO: create a subsection here that stands up a smtp server as 
		# a dependency: https://github.com/aarongough/mini-smtp-server/blob/master/test/unit/mini_smtp_server_test.rb

		# Testing the email mechanisms:
		# Anything recieved with $robot in the To: header should get piped to
		# the sympa queue.  Everything else should get stored in a hash of arrays
		# indexed by message-id (?)
		# Can we make starting the smtp server a dependency (so that we do not
		# run these tests if postfix or something else is already a dependency)

		# To test DMARC all we have to do is subscribe users with known DMARC 
		# entries like gmail.com, yahoo.com, and aol.com.  The DNS lookup will be
		# done for thise servers and the DMARC munging should take place.

	end
end
