##############################################################################
# test_web_sympa.rb
#
# This is a test suite for the wwsympa.fcgi web interface. You should run 
# theese tests like so: ruby test_web_sympa.rb
#
# In order for these tests to run successfully you must use the dbi-dbrc
# library, and create an entry for 'test_wwsympa'. The user name should include
# the full domain, and the driver should be set to the URL:
#
# test_wwsympa postmaster@foo.org xxx http://foo.bar.org/sympa
#
# For all tests to complete successfully, you must use listmaster credentials.
##############################################################################


require 'mechanize'
require 'dbi/dbrc'

class SympaTestSuite
	attr_reader :info
	attr_reader :url
	attr_reader :list
	attr_reader :user
	attr_reader :robot
	attr_reader :page
	attr_reader :error
	attr_reader :browser

	def initialize
		@error = ''
		@list = 'testlist-' + (0..8).map{ rand(10) }.join

		# Get the settings from the db
		@info = DBI::DBRC.new('test_wwsympa')
		
		@url  = info.driver
		@user  = info.user
		@pass  = info.passwd
		@robot = @url.gsub(/https?:\/\/([^\/]+)\/.*/, '\1')

		# Setup a logged in mech instance to the site
		@browser = Mechanize.new {|a| a.ssl_version, a.verify_mode = 'SSLv3', OpenSSL::SSL::VERIFY_NONE}
		@browser.redirect_ok = false
	end

	def login
		@page = @browser.get @url
		form = @page.forms.first
		form.email = @user
		form.passwd = @pass
		@page = form.submit
		@browser.redirect_ok = true

		# if we don't get a 302 then we failed
		if @page.code != "302"
			@error = "username/password combination is invalid"
			return false
		end
		return true
	end

	# TODO: do some sanity checking here
	def logout
		self.browser.post(self.url, {
			'action' => "logout", 
			'action_logout' => "Logout",
			'previous_action' => ""
		})
	end

	def self.finalizer(bar)
		self.logout
	end
end

if __FILE__ == $0
	# Do something.. run tests, call a method, etc. We're direct.
	gem "minitest"
	require 'minitest/autorun'
	class TestSympaTestSuite < Minitest::Test
		def test_exisiting_dot_dbrc
			sts = SympaTestSuite.new
			assert_instance_of DBI::DBRC, sts.info
		end
		def test_sympa_url
			sts = SympaTestSuite.new
			refute_nil sts.url
		end
		def test_sympa_user
			sts = SympaTestSuite.new
			refute_nil sts.user
		end
		def test_sympa_robot
			sts = SympaTestSuite.new
			refute_nil sts.robot
		end
		def test_sympa_browser
			sts = SympaTestSuite.new
			puts sts.browser
			refute_nil sts.browser
		end
		def test_sympa_login
			sts = SympaTestSuite.new
			refute_equal sts.login, false
		end
	end

end
