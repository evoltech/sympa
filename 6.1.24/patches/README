Patches
========

01_rename_lists_no_pending
--------------------------
prevents lists going in to 'pending' status if they are renamed.
https://labs.riseup.net/code/issues/4066

02_disable_copy_list
--------------------
disables the ability to copy an existing list at the perl library level. this is also done on the 
template level (which is not included in this patch).
https://labs.riseup.net/code/projects/lists/activity?from=2012-10-05

03_disable_latest_and_active_lists
-----------------------------------
rss requests to active_lists and latest_lists crash wwsympa. disable for now (should make a real patch one day)

04_fix_search_list.patch
---------------------------
patches do_search_list so that it calls get_lists_db directly with SQL conditions to limit the number of lists that get_lists has to loop through.
https://sourcesup.renater.fr/tracker/index.php?func=detail&aid=9341&group_id=23&atid=167

05_disable_latest_and_active_lists.patch
-----------------------------------------
we still need to disable listing of latest and active lists, since these operations still time out.
TODO; need to file a bug for this and press the issue upstream

06_2685_owner_address_set_to_special_address.patch
--------------------------------------------------
do_edit_list in wwsympa.fcgi needs to be patched to make sure that special list addresses can not be ued as list owner addresses.  See https://labs.riseup.net/code/issues/2685 for more info

07_var-lock-subsys_missing_prevents_startup.patch
-------------------------------------------------
This fixes a bug in the init script for Debian Wheezy systems

08_2821_max_list_dispatch.patch
-------------------------------
Adds a feature to set hard limits on the size of lists that can send mail.  This is controlled by max_list_dispatch and max_list_dispatch_from_subscriber.  See https://labs.riseup.net/code/issues/2821 for more info.

09_1913_email_validation.patch
------------------------------
Adds configureable email validations provided by Email::Valid.  See https://labs.riseup.net/code/issues/1913 for more info.

10_3981_forbid_weak_passwords.patch
-----------------------------------
Adds configureable support for assuring the strength of users passwords.  See https://labs.riseup.net/code/issues/3981 for more info.

11_1187_full_name_removeal
--------------------------
First part of removing Full Names from the sympa database.  This patch removes the possibilit for users to change their names on the preferences page.  See https://labs.riseup.net/code/issues/1187 for more info.

11_1187_full_name_removeal.2
----------------------------
The second part of removing fulle names from the sympa database.  This patch removes the ability of list admins and owners from adding a name for the user from the 'Manage List Members' page.  See https://labs.riseup.net/code/issues/1187 for more info.
